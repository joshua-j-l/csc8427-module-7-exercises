public class Horse extends Animal {

    //constructor
    public Horse(String name) {
        super(name);
        this.species = "Horse";
        this.numLegs = 4;

    }
}
