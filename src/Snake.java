public class Snake extends Animal{

    public Snake(String name)  {
        super(name);
        this.species = "Snake";
        this.numLegs = 0;
    }
}
