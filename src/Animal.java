public class Animal {

    protected String name;
    protected String species;
    protected int numLegs;

    public Animal(String name) {
        this.name = name;
        this.species = species;
        this.numLegs = numLegs;
    }

    // getters
    public String getName() {
        return name;
    }
    public String getSpecies() {
        return species;
    }
    public int getNumLegs() {
        return numLegs;
    }

    public void describe(){
        if(numLegs != 0) {
            System.out.println("My name is " + name + ", I am a " + species + " and I have " + numLegs + " legs.");
        }
        else {
            System.out.println("My name is " + name + ", I am a " + species + " and I have no legs.");

        }
    }



    //setters
    public void setName(String name) {
        this.name = name;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }



}
