public class Budgie extends Animal {

    public Budgie(String name) {
        super(name);
        this.species = "Budgie";
        this.numLegs = 2;
    }
}
