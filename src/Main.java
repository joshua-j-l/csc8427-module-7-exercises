import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        //create animals
        Horse henry = new Horse("Henry");
        Budgie boris = new Budgie("Boris");
        Snake snakey = new Snake("Snakey");

        ArrayList<Animal> animalList = new ArrayList<>();

        animalList.add(henry);
        animalList.add(boris);
        animalList.add(snakey);

        for (Animal a : animalList){
            a.describe();
        }



        // add to array list
        //cycle through arraylist and use describe


    }
}
